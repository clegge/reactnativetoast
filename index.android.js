/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
    Button,
  View
} from 'react-native';

import LeggeToastAndroid from './LeggeToastAndroid';
import ImgPicker from './ImgPicker';


export default class RNToast extends Component {

    constructor(props) {
        super(props);
        this.openImagePicker = this.openImagePicker.bind(this);
        this.state = {
            imgUri: 'Waiting for you to press the button.'
        };
    }



  openImagePicker(){
      ImgPicker.openSelectDialog(
          {}, // no config yet
          //(uri) => { console.log(uri) },
          (uri) => {
              this.setState({
                  imgUri: uri
              }) },
          //(error) => { console.log(error) }
          (error) => {
              this.setState({
                  imgUri: error
              }) },
      );
  }

  render() {
      console.log("render")
      LeggeToastAndroid.show('Legge Rocks', LeggeToastAndroid.SHORT);

    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
          <Text style={styles.instructions} >{this.state.imgUri}</Text>
          <Button
              onPress={this.openImagePicker}
              title="Open Img Picker"
              color="#841584"/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
      fontSize: 20,
    textAlign: 'center',
    color: '#333333',
    marginBottom: 10,
  },
});

AppRegistry.registerComponent('RNToast', () => RNToast);
